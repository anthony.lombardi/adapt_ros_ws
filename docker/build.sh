# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

SERVER="$( bash ${DIR}/GET_SERVER.sh )"
NAME=adapt_dev
TAG="latest"
IMAGENAME="${SERVER}${NAME}:${TAG}"


(docker build -t $IMAGENAME -f $DIR/Dockerfile .)
