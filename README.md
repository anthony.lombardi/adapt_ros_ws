# ADAPT Payload ROS Workspace

ROS workspace that runs the ADAPT processing on Jetson hardware.

See the [ADAPT project website](https://kitware.github.io/adapt/provisioning/) for more details about the project or the and its [provisioning guide](for more details about the project or the ) for help getting started.

Make sure to init git sub-modules in order to pull in the requirements

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.

# Sub-projects

## advanced_navigation_driver

The Advanced Navigation driver is from https://github.com/ros-drivers/advanced_navigation_driver and retains its original license.

The source has been modified to use a different serial interface that has a compatible license and to grab additional RAW packet information.