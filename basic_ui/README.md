# Basic UI ADAPT container

This is a container that has everything needed to communicate with the ADAPT payload from a separate PC (base station).

1. Edit the base-station's /etc/hosts file so that the remote computer running the ADAPT ROS stack has its hostname and IP address for your network configuration. Note: the remote computer will need your computer's hostname and IP in it's /etc/hosts file.

Example of base-station /etc/hosts:
```
127.0.0.1 localhost
127.0.1.1 basestation
100.0.0.20 xaviertx
```
Example of remote Xavier /etc/hosts:
```
127.0.0.1 localhost
127.0.1.1 xaviertx
100.0.0.5 basestation
```

2. Run the build.sh script to create the docker container.

3. Finally run the start_ui.sh script to run it.