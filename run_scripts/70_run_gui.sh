#!/bin/bash
# Determine dirname of this file/directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SESSION="adapt_gui"

# catch exiting the tmux session
trap "{
        echo Stopping TMUX Session;
        tmux kill-session -t ${SESSION};
        exit 0; 
      }" EXIT

echo "$(date) Starting $SESSION"
tmux new -d -s $SESSION
i=1

# GUI
echo "Starting GUI."
tmux new-window -t $SESSION:${i} -n 'GUI'
tmux send-keys "source ../activate_ros.bash" C-m
tmux send-keys "export ROS_MASTER_URI=\"http://xaviertx:11311/\"" C-m
tmux send-keys "roslaunch --wait wxpython_gui system_control_panel.launch" C-m

echo "Running, press C^c to exit..."

sleep infinity

#echo "select tmux window:"
#tmux select-window -t $SESSION:1

#echo "attach tmux:"
# Bring up the tmux session
#tmux attach -t $SESSION
