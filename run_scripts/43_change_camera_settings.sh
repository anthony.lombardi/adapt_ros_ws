echo '!!!! NOTE: ROS node should not be connected !!!'
width=1024
height=768
pf='BayerRG8'
# low res mono for 23Hz
#gc_config adaptcam 'Width=1024'
#gc_config adaptcam 'Height=768'
#gc_config adaptcam 'PixelFormat=Mono8'
# medium res
#gc_config adaptcam 'Width=2736'
#gc_config adaptcam 'Height=1824'
#gc_config adaptcam 'PixelFormat=RGB8Packed'
# full res color ~= 4Hz
#gc_config adaptcam 'Width=5472'
#gc_config adaptcam 'Height=3648'
#gc_config adaptcam 'PixelFormat=BayerRG8'
echo "width=${width}"
echo "height=${height}"
echo "pixelFormat=${pf}"
gc_config adaptcam "PixelFormat=${pf}"
gc_config adaptcam "Width=${width}"
gc_config adaptcam "Height=${height}"

# this setting will speed up Hz but isn't always RW:
# ISP Off is only supported with uninterpolated formats (Mono and Bayer)
gc_config adaptcam 'IspEnable=0'
# ptp settings
gc_config adaptcam 'GevIEEE1588=0' # turn this off to reset it
sleep 2
gc_config adaptcam 'GevIEEE1588Mode=SlaveOnly'
gc_config adaptcam 'GevIEEE1588=1'

#trigger settings
#gc_config adaptcam 'TriggerMode=Off'
gc_config adaptcam 'TriggerMode=On'

gc_config adaptcam 'LineSelector=Line0'

gc_config adaptcam 'LineMode=Input'
gc_config adaptcam 'TriggerSelector=FrameStart'

gc_config adaptcam 'TriggerSource=Line0'
gc_config adaptcam 'TriggerDelay=400' #us
gc_config adaptcam 'TriggerActivation=RisingEdge'

# exposure control
# default is 100us
gc_config adaptcam 'AutoExposureExposureTimeLowerLimit=233'
# default is 15000us
gc_config adaptcam 'AutoExposureExposureTimeUpperLimit=1000'

echo 'completed'
