#! /bin/bash
# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

wget https://data.kitware.com/api/v1/file/63ff896a7b0dfcc98f66a936/download -O $DIR/segmentation.ckpt
