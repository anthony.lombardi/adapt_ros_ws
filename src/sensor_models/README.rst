Camera Models
=============

This package handles operations related to modeling the projection properties of imaging sensors. Features of this package include:

- Modeling of several camera architectures including standard hard-mounted cameras, pan/tilt/zoom cameras, and virtual azimuth/elevation cameras
- Reading from and writing to a yaml configuration file for each camera, which includes all intrinsic parameters, mounting position and orientation, and relevant ROS topic names
- Projecting arbitrary world points into a camera's image and uprojecting image coordinates into a ray in the world
- Synthesizing a view from a real or virtual camera given an image or images from other cameras (i.e., image_renderer.py)

camera_models.py
----------------

This module defines the projection properties of several imaging sensor architectures. The camera model object for each sensor architecture inherits from the base class Camera. Therefore, all sensor architecture objects share the same set of attributes and methods documented in the base Camera class, which allows complicated projection operations to be abstracted into a common interface.

All camera objects are notionally attached to a navigation coordinate system frame, which evolves as a function of time. An ins_service ROS topic should be provided during initialization to allow querying of this navigation coordinate system state as a function of time. Objects for time-varying sensors, such as a pan/tilt/zoom camera, whose internal properties (i.e., pan/tilt/zoom) evolve with time, maintain a catalog of state that can be queried as a function of time.

image_renderer.py
-----------------

This module captures functionality related to warping imagery from one camera, or set of cameras, into another.

/config
-------

The config directory stores the camera model definitions (i.e., yaml configuration files) for each camera in the system. The configuration files capture all detailed about the cameras in the system need to conduct projection and rendering operations.
