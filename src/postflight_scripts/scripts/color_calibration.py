#!/usr/bin/env python
from __future__ import division, print_function
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt

import colour

from colour_checker_detection import (
    EXAMPLES_RESOURCES_DIRECTORY,
    SETTINGS_SEGMENTATION_COLORCHECKER_CLASSIC,
    colour_checkers_coordinates_segmentation,
    detect_colour_checkers_segmentation)
from colour_checker_detection.detection.segmentation import (
    adjust_image)

colour.plotting.colour_style()

colour.utilities.describe_environment();


bridge = CvBridge()


bag_fname = '/home/user/adapt_ws/data/2022-04-21-14-24-57.bag'
imagery_topic = '/image_raw'
imu_topic = '/an_device/Imu'
gps_topic = ' /an_device/NavSatFix'
filter_status_topic = '/an_device/FilterStatus'
ins_system_status_topic = '/an_device/SystemStatus'
out_dir = os.path.splitext(bag_fname)[0]
