#!/usr/bin/env python
"""
explode imagery
"""
from __future__ import division, print_function
import os
import numpy as np
import cv2
import subprocess

res_x = 1000
res_y = 1000

speed = 100  # pixels per second


out_dir = '/home/local/KHQ/matt.brown/libraries/adapt_ros_ws/data'

fname = '%s/bars_%0.f.avi' % (out_dir, speed)
writer = cv2.VideoWriter(fname,
                         cv2.VideoWriter_fourcc(*'MJPG'), 120,
                         (res_x, res_y), True)
for dx in np.linspace(5, res_x - 5, int(res_x//speed)):
    dx = int(dx)
    img = np.zeros((res_y, res_x, 3), dtype=np.uint8)
    img[:, dx:dx+5, :] = img[:, dx:dx+5, :] = img[:, dx:dx+5, :] = 255
    img[:, -(dx+5):-dx, :] = img[:, -(dx+5):-dx, :] = img[:, -(dx+5):-dx, :] = 255
    writer.write(img[:, :, ::-1])

writer.release()

# FFMPEG video
video_out_fname = '%s/bars_%0.f.mp4' % (out_dir, speed)
# Call ffmpeg to explode the video file to an image directory.
ffmpeg_call = ' '.join(['ffmpeg -i', fname,
                        '-vcodec libx265 -crf 28', video_out_fname])

subprocess.call(ffmpeg_call, shell=True)
os.remove(fname)
