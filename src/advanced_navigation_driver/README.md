# advanced_navigation_driver
Note: this is a forked version of https://github.com/ros-drivers/advanced_navigation_driver.git

Changes:
* Addition of Odometry msg
* Changed from rs232 library to serial
* added topics for RawImu and RawGNSS packets (28 & 29)

---------------------------------------
Driver for the range of INS systems from Advanced Navigation

Packet to Published Message Example

Copyright 2017, Advanced Navigation

This is an example using the Advanced Navigation Spatial SDK to create a ROS driver that reads and decodes the anpp packets (in this case packet #20 and packet #27) and publishes the information as ROS topics / messages.

It should work on all Advanced Navigation INS devices.

This example has been developed and tested using Ubuntu Linux v16.04 LTS and ROS Lunar. Installation instructions for ROS can be found here: http://wiki.ros.org/lunar/Installation/Ubuntu.

If you require any assistance using this code, please email support@advancednavigation.com.au.

Installation, build, device configuration, and execution instructions can be found in the file "Advanced Navigation ROS Driver Notes.txt".

---------------------------------------
## Odometry message
An instance of nav_msgs/Odometry encoding the Kalman filtered navigation solution provided by the INS:

- pose.pose.position.x - longitude in decimal degrees.
- pose.pose.position.y - latitude in decimal degrees.
- pose.pose.position.z - height above WGS84 ellipsoid in meters.

