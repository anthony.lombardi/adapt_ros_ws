# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jan 28 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Base Layer", pos = wx.DefaultPosition, size = wx.Size( 1060,663 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.base_layer_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer1.Add( self.base_layer_panel, 1, wx.EXPAND |wx.ALL, 5 )
		
		self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )
		
		
		self.m_panel3.SetSizer( bSizer3 )
		self.m_panel3.Layout()
		bSizer3.Fit( self.m_panel3 )
		bSizer1.Add( self.m_panel3, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.zoom_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.follow_system_checkbox = wx.CheckBox( self.zoom_panel, wx.ID_ANY, u"Follow System", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.follow_system_checkbox.SetValue(True) 
		bSizer2.Add( self.follow_system_checkbox, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.show_ins_coord_sys_checkbox = wx.CheckBox( self.zoom_panel, wx.ID_ANY, u"Show INS Coordinate System", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.show_ins_coord_sys_checkbox.SetValue(True) 
		bSizer2.Add( self.show_ins_coord_sys_checkbox, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer2.AddSpacer( ( 50, 0), 0, wx.EXPAND, 5 )
		
		self.m_staticText2 = wx.StaticText( self.zoom_panel, wx.ID_ANY, u"Zoom", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )
		self.m_staticText2.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer2.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.zoom_slider = wx.Slider( self.zoom_panel, wx.ID_ANY, 1000, 0, 10000, wx.DefaultPosition, wx.DefaultSize, wx.SL_HORIZONTAL )
		bSizer2.Add( self.zoom_slider, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.zoom_spin_button = wx.SpinButton( self.zoom_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.zoom_spin_button, 0, wx.ALL, 5 )
		
		self.zoom_static_text = wx.StaticText( self.zoom_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.zoom_static_text.Wrap( -1 )
		bSizer2.Add( self.zoom_static_text, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		self.zoom_panel.SetSizer( bSizer2 )
		self.zoom_panel.Layout()
		bSizer2.Fit( self.zoom_panel )
		bSizer1.Add( self.zoom_panel, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Load Image", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem1 )
		
		self.m_menubar1.Append( self.m_menu1, u"File" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.follow_system_checkbox.Bind( wx.EVT_CHECKBOX, self.on_follow_system_checkbox )
		self.show_ins_coord_sys_checkbox.Bind( wx.EVT_CHECKBOX, self.on_show_ins_coord_sys_checkbox )
		self.Bind( wx.EVT_MENU, self.on_load_image, id = self.m_menuItem1.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_follow_system_checkbox( self, event ):
		event.Skip()
	
	def on_show_ins_coord_sys_checkbox( self, event ):
		event.Skip()
	
	def on_load_image( self, event ):
		event.Skip()
	

